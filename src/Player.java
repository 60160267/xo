public class Player {

	private char name;
	private int win;
	private int draw;
	private int lose;

	Player (char name ){
		this.name = name;
	}
	
	char getName() {
		return name;
	}
	
	void setName(char name) {
        this.name = name;
    }
	
	int getWin() {
		return win;
	}
	
	int getDraw() {
		return draw;
	}
	
	int getLose() {
		return lose;
	}
	
	void win() {
		win++;
	}
	
	void draw() {
		draw++;
	}
	
	void lose() {
		lose++;
	}
	

}
