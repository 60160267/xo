public class Board {

	char table[][] = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	private Player o;
	private Player x;
	private Player winner;
	private Player current;
	private int turnCount;

	Board(Player x, Player o) {
		this.o = o;
		this.x = x;
		current = x;
		winner = null;
		turnCount = 0;
	}

	boolean isFinish() {

		if ((table[0][0] == 'X' && table[0][1] == 'X' && table[0][2] == 'X')
				|| (table[1][0] == 'X' && table[1][1] == 'X' && table[1][2] == 'X')
				|| (table[2][0] == 'X' && table[2][1] == 'X' && table[2][2] == 'X')
				|| (table[0][0] == 'X' && table[1][0] == 'X' && table[2][0] == 'X')
				|| (table[0][1] == 'X' && table[1][1] == 'X' && table[2][1] == 'X')
				|| (table[0][2] == 'X' && table[1][2] == 'X' && table[2][2] == 'X')
				|| (table[0][0] == 'X' && table[1][1] == 'X' && table[2][2] == 'X')
				|| (table[0][2] == 'X' && table[1][1] == 'X' && table[2][0] == 'X')) {
			winner = x;
			x.win();
			o.lose();
			return true;
		} else if ((table[0][0] == 'O' && table[0][1] == 'O' && table[0][2] == 'O')
				|| (table[1][0] == 'O' && table[1][1] == 'O' && table[1][2] == 'O')
				|| (table[2][0] == 'O' && table[2][1] == 'O' && table[2][2] == 'O')
				|| (table[0][0] == 'O' && table[1][0] == 'O' && table[2][0] == 'O')
				|| (table[0][1] == 'O' && table[1][1] == 'O' && table[2][1] == 'O')
				|| (table[0][2] == 'O' && table[1][2] == 'O' && table[2][2] == 'O')
				|| (table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O')
				|| (table[0][2] == 'O' && table[1][1] == 'O' && table[2][0] == 'O')) {
			winner = o;
			o.win();
			x.lose();
			return true;
		}
		if (turnCount == 9) {
			System.out.println("Draw!!");
			o.draw();
			x.draw();
			return true;
		}
		return false;
	}

	char[][] getTable() {
		return table;
	}

	Player getCurrent() {
		return current;
	}
	
	Player getWinner() {
		return winner;
	}

	void setTable(int row, int col) {
		table[row][col] = current.getName();
		turnCount++;
	}

	void swichTurn() {

		if (current.getName() == 'X') {
			current = o;
		} else {
			current = x;
		}

	}
	void setCount() {
		turnCount = 0;
	}

}
