import java.util.*;

public class Game {

	private Board board;
	private Player x;
	private Player o;

	Game() {
		o = new Player('O');
		x = new Player('X');
		board = new Board(x, o);
	}

	void Play() {
		showWelcome();
		while (true) {
			while (true) {
				showTable();
				showTurn();
				input();
				if (board.isFinish() == true) {
					showTable();
					showResult();
					showBye();
					showStat();
					break;
				}
				
			}
			clearTable();
		}
	}

	static void showWelcome() {
		System.out.println("Welcome To OX Game");
	}

	void showTable() {
		System.out.println("  1 2 3");
		char[][] table = board.getTable();
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1 + " ");
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
	}

	void showTurn() {
		System.out.println(board.getCurrent().getName() + " Turn...");
	}

	private void input() {
		Scanner key = new Scanner(System.in);
		System.out.print("Please input Row Col : ");
		int row = key.nextInt();
		int col = key.nextInt();
		System.out.println();
		if (row - 1 >= 0 && row - 1 <= 2 && col - 1 >= 0 && col - 1 <= 2) {
			if (board.getTable()[row - 1][col - 1] == '-') {
				board.setTable(row - 1, col - 1);
				board.swichTurn();
			} else {
				System.out.println("Error this index is not -. Please Input again.");
			}
		} else {
			System.out.println("Error Index out of!! Please Input again.");
		}

	}

	private void showResult() {
		System.out.println(board.getWinner().getName() + " Win!!");
	}

	private void showBye() {
		System.out.println("Bye bye!!");
		System.out.println();
	}

	private void showStat() {
		System.out.println("X Stat : Win " + x.getWin() + " Lose " + x.getLose() + " Draw " + x.getDraw());
		System.out.println("O Stat : Win " + o.getWin() + " Lose " + o.getLose() + " Draw " + o.getDraw());
		System.out.println();
	}

	private void clearTable() {
		board.setCount();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board.table[i][j] = '-';
			}
		}
	}

}
